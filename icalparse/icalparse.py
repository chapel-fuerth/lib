# Copyright 2017 Bo Maryniuk <bo@suse.de>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without
# limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
try:
    import icalendar
except ImportError:
    icalendar = None


class ICalendarGeneralException(Exception): pass


class ICalendarStructure(object):
    '''
    iCalendar parser
    '''
    class Event(object):
        '''
        Event structure.
        '''
        def __init__(self, component):
            self.__data = type('data', (), {})
            for field, renamed in [('summary', None), ('description', None),
                                   ('location', None), ('dtstart', 'start'),
                                   ('dtend', 'end'), ('exdate', 'expires')]:
                ref = component.get(field)
                if ref:
                    if field in ['dtstart', 'dtend']:
                        val = ref.dt
                    elif field in ['exdate']:
                        val = [x.dt for x in ref.dts]
                    else:
                        val = ref
                else:
                    val = None
                setattr(self.__data, renamed or field, val)

        def __getattr__(self, item):
            '''
            Get value of the data by key.
            :param item:
            :return:
            '''
            return hasattr(self.__data, item) and getattr(self.__data, item) or None

    def __init__(self, path=None, content=None):
        if icalendar is None:
            raise ICalendarGeneralException('iCalendar missing system dependencies')
        if path and not os.path.exists(path):
            raise FileNotFoundError('iCalendar file "{0}" was not found'.format(path))

        try:
            self._cal = icalendar.Calendar.from_ical(path and open(path).read() or content)
        except ValueError as err:
            raise ICalendarGeneralException(err)
        self._data = []
        self._parse()

    def _parse(self):
        '''
        Parse iCalendar into a structure
        :return:
        '''
        for component in self._cal.walk():
            if component.name == 'VEVENT':
                self._data.append(self.Event(component))


def main():
    ical = ICalendarStructure(content=open('testcal.ics').read())
    for event in ical._data:
        print('-' * 80)
        print(event.summary, "\n\t", event.dtstart, ":", event.dtend)
        print(event.description or 'No idea what is this meeting is for')
        print("Located at", event.location)

if __name__ == '__main__':
    main()
